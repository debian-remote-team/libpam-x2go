libpam-x2go (0.0.2.0-3) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards-Version: to 4.4.1. No changes needed.
    + Add Rules-Requires-Root: field and set it to no.

  [ Helmut Grohne ]
  * debian/patches:
    + Add 1001_dont_abuse_AC_CHECK_FILES.patch. Fix FTCBFS. (Closes: #941593).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 04 Dec 2019 08:14:10 +0100

libpam-x2go (0.0.2.0-2) unstable; urgency=medium

  * debian/control: Update Maintainer: field.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 09 May 2018 01:01:28 +0200

libpam-x2go (0.0.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Update Section: field (misc -> admin).
    + Bump Standards-Version: to 4.1.4. No changes needed.
    + Update Vcs-*: fields. Packaging Git has been migrated to salsa.debian.org.
    + Drop alternative x2goclient from Suggests: field. Update LONG_DESCRIPTION
      of libpam-x2go.
    + Add bin:pkg libpam-x2go-dev.
    + Add to B (libpam-x2go): lightdm-remote-session-x2go (<< 0.0.2.0).
  * debian/rules:
    + Run CDBS's list-missing after build.
    + Copy README.md to README for build.
    + Enable unit tests.
    + Fix build cruft removal.
  * debian/copyright:
    + Add auto-generated copyright.in file.
    + Use secure URI for copyright format reference.
    + Update copyright attributions.
  * debian/watch:
    + Use secure URL to obtain upstream sources.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 09 May 2018 00:35:57 +0200

libpam-x2go (0.0.1.1-2) unstable; urgency=medium

  * Upload to unstable.

  * debian/control:
    + Set Priority: from extra to optional to comply with Debian Policy 4.1.0.
    + Bump Standards-Version: to 4.1.0.
  * debian/{control,compat}:
    + Bump DH version level to 10 (and bump B-D to >= 10).

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 01 Oct 2017 20:32:13 +0200

libpam-x2go (0.0.1.1-1) experimental; urgency=low

  * Initial upload to Debian. (Closes: #864871).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 16 Jun 2017 11:52:16 +0200
